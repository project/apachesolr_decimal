// $Id$

/**
 * @file
 * Provides the ApacheSolr Decimal Number jQuery slider settings and actions.
 */

Drupal.theme.prototype.delayDecimalSubmit = function(){
  window.setTimeout(function(){
    $("#apachesolr-decimal-range-form").submit();
  }, 500);
};

Drupal.theme.prototype.loadDecimalSlider = function(){
  $("#apachesolr-decimal-range-form #decimal-range-slider").slider({
    range: true,
    min: parseInt(Drupal.settings.apacheSolrDecimalMin),
    max: parseInt(Drupal.settings.apacheSolrDecimalMax),
    values: [parseInt(Drupal.settings.apacheSolrDecimalMin), parseInt(Drupal.settings.apacheSolrDecimalMax)],
    slide: function(event, ui){
      $("#edit-decimal-range-from").val(ui.values[0]);
      $("#edit-decimal-range-to").val(ui.values[1]);
    }
  });
  $(".block-apachesolr_decimal .form-submit").hide();
  $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 0, $("#edit-decimal-range-from").val());
  $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 1, $("#edit-decimal-range-to").val());
  $("#edit-decimal-range-from").keyup(function(){
    if (this.value <= $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 1)) {
      $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 0, this.value);
    }
    else {
      $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 0, $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 1));
    }
  });
  $("#edit-decimal-range-to").keyup(function(){
    if (this.value >= $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 0)) {
      $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 1, this.value);
    }
    else {
      $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 1, $("#apachesolr-decimal-range-form #decimal-range-slider").slider("values", 0));
    }
  });
  $("#edit-decimal-range-from").keyup(function(){
    Drupal.theme('delayDecimalSubmit');
  });
  $("#edit-decimal-range-to").keyup(function(){
    Drupal.theme('delayDecimalSubmit');
  });
  // we add a .mouseover check to prevent unwanted submitting (e.g. caused by the the ajax "I like" flags)
  // @todo find a more stable solution for the following
  $("#apachesolr-decimal-range-form #decimal-range-slider").mouseenter(function(){
    $("#apachesolr-decimal-range-form #decimal-range-slider").bind("slidechange", function(event, ui){
      Drupal.theme('delayDecimalSubmit');
    });
  });
  $("#apachesolr-decimal-range-form #decimal-range-slider").mouseleave(function(){
    $("#apachesolr-decimal-range-form #decimal-range-slider").unbind("slidechange");
  });
}

Drupal.behaviors.apachesolr_decimal = function(context){
  Drupal.settings.apacheSolrDecimalMin = $("#edit-decimal-range-min").val();
  Drupal.settings.apacheSolrDecimalMax = $("#edit-decimal-range-max").val();
  Drupal.settings.apacheSolrDecimalFilters = $('#edit-ajax-filters').val();
  
  Drupal.theme('loadDecimalSlider');
}
